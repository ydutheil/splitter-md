import pyjapc

import functions as fs
import numpy as np
import matplotlib
from functools import partial

 
import matplotlib.pyplot as plt
import pandas as pd

plt.ion()

# window of the scan itself
fig_scan, ax_scan = plt.subplots(2, 1, figsize=(9, 5), sharex=True)
fig_scan.canvas.set_window_title('Scan')
ax_scan[0].set_ylabel('BLM 211742 RLS')
ax_scan[1].set_ylabel('BSIs')
ax_scan[0].grid()
ax_scan[1].grid()

ax_scan[1].plot([], [], 'rx', label='t6')
ax_scan[1].plot([], [], 'gx', label='t4')
ax_scan[1].plot([], [], 'bx', label='t2')


setRun = True
USER = 'SPS.USER.SFTPRO1'

a = np.hstack([np.arange(0, 13.1, 1), np.arange(-17, 0, 2), np.arange(25, 13, -2)])
a = np.arange(-2, 2, 0.1)
a.sort()
scan_iterable = a
nrep = 1
params_to_get = [
    "SPS.BCTDC.51454/Acquisition",
    *[key for key in knob_def],
    'BLRSPS_BA80-SP.BLML.211742.RLS_MSSB_TDC2-UCAP/ExpertAcquisition#beamLosses_gray',
    'SEM_CONCENTRATOR_TT20/BeamIntensity#intensity'
    ]

japc = pyjapc.PyJapc(USER, noSet=(not setRun))
japc.rbacLogin()
    
    
    
exp = fs.Experiment(
    japc,
    params_to_get,
    scan_iterable, nrep, knob_def,
    fig_scan, ax_scan)

japc.subscribeParam('SEM_CONCENTRATOR_TT20/BeamIntensity#intensity',
                    partial(fs.measurement, exp))

japc.startSubscriptions()
print('subscribed and running')
