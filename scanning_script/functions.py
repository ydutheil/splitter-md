import numpy as np
import matplotlib.pyplot as plt
import time
import random as rdm
import pandas as pd

from dateutil.parser import parse


class Experiment:

    def __init__(self,
                 japc, 
                 params_to_get,
                 scan_iterable, nrep, knob_def, 
                 fig_scan, ax_scan):
        self.japc = japc

        self.params_to_get = params_to_get

        self.scan_iterable = scan_iterable
        self.nrep = nrep
        self.knob_def = knob_def

        self.fig_scan = fig_scan
        self.ax_scan = ax_scan

        self.id_meas = 0
        self.id_rep = 0
        self.data = []
        self.line = []
        self.current_knob = 0

        print('\n\n\n Initial knob values')
        self.initial_knob = {}
        for key in knob_def:
            val = japc.getParam(key)
            strength = val[1][2]
            self.initial_knob[key] = strength
            print(key, strength)
        


    def get_measurement(self):

        for param in self.params_to_get:
            if isinstance(param,list):
                val = self.japc.getParam(param[0], timingSelectorOverride=param[1])
                param=param[0]
            else:
                val = self.japc.getParam(param)
            self.line.append(val)

    def next_meas(self):
        # check that current measurement is different from previous
        try:
            if False:  # condition that the meansurement is not good
                print("current measurement has repeated, ignoring")
                self.line = []
                return
        except IndexError:
            pass

        # check that there is beam
        bct = self.line[0]['slowExtInt']-self.line[0]['dumpInt']
        bct = max(0, bct)
        if bct < 10:
            self.line = []
            print('not enough intensity, dropping cycle')
            return
            
        self.update_plot(self.line)
        self.line.append(self.current_knob)
        self.data.append(self.line)
        self.line = []

        self.id_rep += 1
        if self.id_rep >= self.nrep:
            self.id_rep = 0
            self.id_meas += 1
            if self.id_meas >= len(self.scan_iterable):
                self.set_params(0)
                self.end_measurement()
                print('****** measurement ended')
            else:
                self.set_params(self.scan_iterable[self.id_meas])


            

    def set_params(self, knob_fraction):
        print('setting to knob fraction ', knob_fraction)
        self.current_knob = knob_fraction
        for key in self.knob_def:
            value = self.knob_def[key]*knob_fraction + self.initial_knob[key]
            self.set_function(key, value)
            print('setting {:40s} to  {:+12.4e}'.format(key, value))

    def set_function(self, address, value):
        array = np.array([
            [ 0.,  2230.,  4460.,  9240., 10020., 10800.],
            [np.NaN, np.NaN, value, value, np.NaN, np.NaN]
            ])
        self.japc.setParam(address, array)

    def end_measurement(self):
        self.japc.stopSubscriptions()

        # now we set the array and do the fit
        columns = [ parm for parm in self.params_to_get]+['knob fraction']
        df = pd.DataFrame(self.data, columns=columns)

        df.to_pickle('scan_' + time.strftime('%Y_%m_%d_%H_%M') + '.pckl')

        self.set_params(0)  # restore starting value

    def update_plot(self, line):
        
        x_val = self.current_knob
        bct = line[0]['slowExtInt']-line[0]['dumpInt']

        blm = line[-2]/bct
        self.ax_scan[0].plot(x_val, blm, 'rx')

        bsi6 = line[-1][53]/bct #t6
        self.ax_scan[1].plot(x_val, bsi6, 'rx')

        bsi4 = line[-1][52]/bct #t4
        self.ax_scan[1].plot(x_val, bsi4, 'gx')

        bsi2 = line[-1][51]/bct #t2
        self.ax_scan[1].plot(x_val, bsi2, 'bx')
        
        self.ax_scan[1].plot(x_val, bsi2+bsi4+bsi6, 'kx')

        self.fig_scan.canvas.draw()



def measurement(exp, parameter_name, new_value, **kwargs):
    print("callback", parameter_name)
    time.sleep(2)



    print('measurement {} , repetition {}'.format(exp.id_meas, exp.id_rep))
    exp.get_measurement()
    exp.next_meas()

    
