#smaller V optics

knob_def = { #position bump, +1mm in vertical smaller beam
    'rmi://virtual_sps/logical.MDLV.2115/K': 10.36e-6,
    'rmi://virtual_sps/logical.MDLV.2116/K': 4.44e-6,
    'rmi://virtual_sps/logical.MDLV.2201.M/K': -12.51e-6,
    'rmi://virtual_sps/logical.MDLV.2204.M/K': 11.46e-6,
    }

knob_def = {#angle bump, 1urad at the splitter smaller beam
    'rmi://virtual_sps/logical.MDLV.2115/K': -20.18e-6/100,
    'rmi://virtual_sps/logical.MDLV.2116/K': 100.16e-6/100,
    'rmi://virtual_sps/logical.MDLV.2201.M/K': -95.66e-6/100,
    'rmi://virtual_sps/logical.MDLV.2204.M/K': 37.79e-6/100,
    }


knob_def = {#posiiton bump in in H
    'rmi://virtual_sps/logical.MBB2113/K': 4.04e-6,
    'rmi://virtual_sps/logical.MPLS.2115/K': 27.91e-6,
    'rmi://virtual_sps/logical.MDAH.2201/K': 30.83e-6,
    'rmi://virtual_sps/logical.MDAH.2303/K': 8.27e-6,
}

knob_def = {#angle bump in H, 1urad
    'rmi://virtual_sps/logical.MBB2113/K': -16.15e-6/100,
    'rmi://virtual_sps/logical.MPLS.2115/K': 6.99e-6/100,
    'rmi://virtual_sps/logical.MDAH.2201/K': 36.10e-6/100,
    'rmi://virtual_sps/logical.MDAH.2303/K': 31.18e-6/100,
}



# nominal optics

knob_def = { #position bump, +1mm in vertical normal beam
    'rmi://virtual_sps/logical.MDLV.2115/K': 10.22e-6,
    'rmi://virtual_sps/logical.MDLV.2116/K': 4.08e-6,
    'rmi://virtual_sps/logical.MDLV.2201.M/K': -12.27e-6,
    'rmi://virtual_sps/logical.MDLV.2204.M/K': 11.43e-6,
    }

knob_def = {#angle bump, 1urad at the splitter normal beam
    'rmi://virtual_sps/logical.MDLV.2115/K': -18.37e-6/100,
    'rmi://virtual_sps/logical.MDLV.2116/K': 104.38e-6/100,
    'rmi://virtual_sps/logical.MDLV.2201.M/K': -95.84e-6/100,
    'rmi://virtual_sps/logical.MDLV.2204.M/K': 39.45e-6/100,
    }

knob_def = {#posiiton bump in in H
    'rmi://virtual_sps/logical.MBB2113/K': 4.22e-6,
    'rmi://virtual_sps/logical.MPLS.2115/K': 27.37e-6,
    'rmi://virtual_sps/logical.MDAH.2201/K': 31.74e-6,
    'rmi://virtual_sps/logical.MDAH.2303/K': 8.44e-6,
}

knob_def = {#angle bump in H, 1urad
    'rmi://virtual_sps/logical.MBB2113/K': -16.14e-6/100,
    'rmi://virtual_sps/logical.MPLS.2115/K':16.02e-6/100,
    'rmi://virtual_sps/logical.MDAH.2201/K': 44.53e-6/100,
    'rmi://virtual_sps/logical.MDAH.2303/K': 33.44e-6/100,
}


# second splitter smallerV optics

knob_def = { #position bump, +1mm in vertical smaller beam  
    'rmi://virtual_sps/logical.MDLV.2201.M/K': 12.47e-6,
    'rmi://virtual_sps/logical.MDLV.2204.M/K': -11.46e-6,
    'rmi://virtual_sps/logical.MDAV.2301.M/K': -5.87e-6,
    'rmi://virtual_sps/logical.MDAV.2305.M/K': 5.17e-6,
    }

knob_def = {#angle bump, 1urad at the splitter smaller beam 
    'rmi://virtual_sps/logical.MDLV.2201.M/K': -37.55e-6/100,
    'rmi://virtual_sps/logical.MDLV.2204.M/K': 84.50e-6/100,
    'rmi://virtual_sps/logical.MDAV.2301.M/K': -69.91e-6/100,
    'rmi://virtual_sps/logical.MDAV.2305.M/K': 17.55e-6/100,
    }


knob_def = {#posiiton bump in in H
    'rmi://virtual_sps/logical.MPLS.2115/K': 13.67e-6,
    'rmi://virtual_sps/logical.MDAH.2201/K': 25.21e-6,
    'rmi://virtual_sps/logical.MDAH.2303/K': -11.28e-6,
    'rmi://virtual_sps/logical.MDAH.2307/K': 23.32e-6,
}

knob_def = {#posiiton bump in in H
    'rmi://virtual_sps/logical.MPLS.2115/K': -65.06e-6/100,
    'rmi://virtual_sps/logical.MDAH.2201/K': -30.48e-6/100,
    'rmi://virtual_sps/logical.MDAH.2303/K': -190.68e-6/100,
    'rmi://virtual_sps/logical.MDAH.2307/K': 194.03e-6/100,
}

# second splitter nominal optics


knob_def = { #position bump, +1mm in vertical smaller beam  
    'rmi://virtual_sps/logical.MDLV.2201.M/K': 12.23e-6,
    'rmi://virtual_sps/logical.MDLV.2204.M/K': -11.43e-6,
    'rmi://virtual_sps/logical.MDAV.2301.M/K': -5.86e-6,
    'rmi://virtual_sps/logical.MDAV.2305.M/K': 5.25e-6,
    }

knob_def = {#angle bump, 1urad at the splitter smaller beam 
    'rmi://virtual_sps/logical.MDLV.2201.M/K': -34.81e-6/100,
    'rmi://virtual_sps/logical.MDLV.2204.M/K': 82.54e-6/100,
    'rmi://virtual_sps/logical.MDAV.2301.M/K': -70.85e-6/100,
    'rmi://virtual_sps/logical.MDAV.2305.M/K': 18.69e-6/100,
    }



knob_def = {#posiiton bump in in H
    'rmi://virtual_sps/logical.MPLS.2115/K': 13.57e-6,
    'rmi://virtual_sps/logical.MDAH.2201/K': 25.75e-6,
    'rmi://virtual_sps/logical.MDAH.2303/K': -11.55e-6,
    'rmi://virtual_sps/logical.MDAH.2307/K': 23.93e-6,
}

knob_def = {#angle bump in in H
    'rmi://virtual_sps/logical.MPLS.2115/K': -62.22e-6/100,
    'rmi://virtual_sps/logical.MDAH.2201/K': -25.33e-6/100,
    'rmi://virtual_sps/logical.MDAH.2303/K': -194.70e-6/100,
    'rmi://virtual_sps/logical.MDAH.2307/K': 203.15e-6/100,
}


Scan BSI T4 BSI.240610

knob_def = {#posiiton bump in in H
    'rmi://virtual_sps/logical.MSSB.2204.M/K': -12.45e-6,
    'rmi://virtual_sps/logical.MBB.2404.M/K': -7.99e-6/100,
}

knob_def = {#posiiton bump in in V
    'rmi://virtual_sps/logical.MDLV.2401.M/K': 370.74e-6,
    'rmi://virtual_sps/logical.MDLV.2402.M/K': -387.25e-6,
}



