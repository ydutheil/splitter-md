import pyjapc
import os
import numpy as np
import logging
import matplotlib.pyplot as plt
import datetime
from matplotlib.animation import FuncAnimation
import pickle
import time
import json
from pybt.myjson.encoder import myJSONEncoder

userName = "SPS.USER.SFTPRO2"

japc = pyjapc.PyJapc(userName)


bsi_name = ['BSIA.210279',
            'BSIB.210279',
            'BSIA.210272',
            'BSIB.210272',
            'BSIC.210272',
            'BSID.210272',
            'BSIA.210278',
            'BSIB.210278',
            'BSIC.210278',
            'BSID.210278',
            'BSI.211626',
            'BSIA.220412',
            'BSI.230705',
            'BSI.230950',
            'BSI.240610',
            'BSI.241149',
            'BSI.241150',
            'BSI.251010',
            'BSI.251247']


ba80_blm = ["BLRSPS_BA80-SP.BLML.211631.LLS_MDLV_TDC2-UCAP",
            "BLRSPS_BA80-SP.BLML.211708.LLS_TCSC_TDC2-UCAP",
            "BLRSPS_BA80-SP.BLML.211713.LLS_MSSB_TDC2-UCAP",
            "BLRSPS_BA80-SP.BLML.211714.RLS_MSSB_TDC2-UCAP",
            "BLRSPS_BA80-SP.BLML.211732.LLS_MSSB_TDC2-UCAP",
            "BLRSPS_BA80-SP.BLML.211741.LLS_MSSB_TDC2-UCAP",
            "BLRSPS_BA80-SP.BLML.211742.RLS_MSSB_TDC2-UCAP",
            "BLRSPS_BA80-SP.BLML.220400.LLS_QTAD_TDC2-UCAP",
            "BLRSPS_BA80-SP.BLML.220436.LLS_TSCS_TDC2-UCAP",
            "BLRSPS_BA80-SP.BLML.220441.LLS_MSSB_TDC2-UCAP",
            "BLRSPS_BA80-SP.BLML.220442.RLS_MSSB_TDC2-UCAP",
            "BLRSPS_BA80-SP.BLML.220469.LLS_MSSB_TDC2-UCAP",
            "BLRSPS_BA80-SP.BLML.220460.LLS_MSSB_TDC2-UCAP",
            "BLRSPS_BA80-SP.BLML.240209.LLS_MDLV_TDC2-UCAP"]

tt20_blm = ["BLRSPS_LSS2-SP.BLML.211006.RLS_QNLD_TT20-UCAP",
            "BLRSPS_LSS2-SP.BLML.210558.RLS_MBE_TT20-UCAP",
            "BLRSPS_LSS2-SP.BLML.211511.RLS.QNLD_TT20-UCAP",
            "BLRSPS_LSS2-SP.BLML.211415.RLS_MBE_TT20-UCAP",
            "BLRSPS_LSS2-SP.BLML.211106.RLS_QNLD_TT20-UCAP",
            "BLRSPS_LSS2-SP.BLML.210330.RLS_MBE_TT20-UCAP",
            "BLRSPS_LSS2-SP.BLML.210916.RLS_MBB_TT20-UCAP",
            "BLRSPS_LSS2-SP.BLML.210305.RLS_MDLV_TT20-UCAP",
            "BLRSPS_LSS2-SP.BLML.210706.RLS_QNLD_TT20-UCAP",
            "BLRSPS_LSS2-SP.BLML.210806.RLS_QNLD_TT20-UCAP",
            "BLRSPS_LSS2-SP.BLML.210280.RLS_QTLD_TT20-UCAP",
            "BLRSPS_LSS2-SP.BLML.211368.RLS_MBB_TT20-UCAP",
            "BLRSPS_LSS2-SP.BLML.211600.RLS.QNLF_TT20-UCAP",
            "BLRSPS_LSS2-SP.BLML.210106.RLS_QTLD_TT20-UCAP",
            "BLRSPS_LSS2-SP.BLML.210222.RLS_MDLV_TT20-UCAP"]

ring_bct = "SPS.BCTDC.31458/Acquisition#slowExtInt"

blm_ba80_dict, blm_tt20_dict, bsi_dict, sum_blm_ba80, sum_blm_tt20, bct_list, timestamp_list = {i: [] for i in ba80_blm},  {i: [] for i in tt20_blm}, {i: [] for i in bsi_name}, [], [], [], []

file_name = "MD_20210811_" + str(datetime.datetime.now())

os.mkdir(file_name)

def callback(param, value, header):

    data_ring_bct = value
    checkFirstUpdate = header['isFirstUpdate']

    if data_ring_bct > -100.:
        if not checkFirstUpdate:
            print('Start data loading!')

            bct_list.append(data_ring_bct)
            print('BCT', len(bct_list), bct_list)

            bsi_name_list = japc.getParam('SEM_CONCENTRATOR_TT20/BeamIntensity#device_name')
            data_bsi = {i: [] for i in bsi_name}
            for i in bsi_name:
                bsi_index = np.where(bsi_name_list == i)[0][0]
                #print(bsi_index)
                data_bsi[i] = japc.getParam('SEM_CONCENTRATOR_TT20/BeamIntensity#intensity')[bsi_index]

            for i in bsi_dict.keys():
                bsi_dict[i].append(data_bsi[i])
                #print("BSI", len(bsi_dict[i]), bsi_dict[i])

            sum_ba80_blm = []
            data_ba80_blm = {i: [] for i in ba80_blm}
            for i in data_ba80_blm.keys():
                data_ba80_blm[i] = japc.getParam(i + '/ExpertAcquisition#beamLosses_gray')
                sum_ba80_blm.append(data_ba80_blm[i])
                #print(i, data_ba80_blm[i])

            for i in blm_ba80_dict.keys():
                blm_ba80_dict[i].append(data_ba80_blm[i])
                #print("BLM", len(blm_ba80_dict[i]), blm_ba80_dict[i])

            sum_tt20_blm = []
            data_tt20_blm = {i: [] for i in tt20_blm}
            for i in data_tt20_blm.keys():
                data_tt20_blm[i] = japc.getParam(i + '/ExpertAcquisition#beamLosses_gray')
                sum_tt20_blm.append(data_tt20_blm[i])
                #print(i, data_tt20_blm[i])

            for i in blm_tt20_dict.keys():
                blm_tt20_dict[i].append(data_tt20_blm[i])
                #print("BLM", len(blm_tt20_dict[i]), blm_tt20_dict[i])

            sum_blm_ba80.append(np.sum(sum_ba80_blm))
            sum_blm_tt20.append(np.sum(sum_tt20_blm))

            data_timestamp = header['cycleStamp']
            timestamp_list.append(data_timestamp)
            print("Timestamp", len(timestamp_list), timestamp_list)

            print('Finished data loading!')

            pickle.dump((blm_ba80_dict, blm_tt20_dict, bsi_dict, data_ring_bct, data_timestamp, sum_blm_ba80, sum_blm_tt20), open(file_name + '/' + file_name + '.p', 'wb'))

            with open(file_name + '/' + file_name + '.json', 'w') as outfile:
                outfile.write(json.dumps((blm_ba80_dict, blm_tt20_dict, bsi_dict, data_ring_bct, data_timestamp, sum_blm_ba80, sum_blm_tt20), cls=myJSONEncoder))

        else:
            logging.warning("Skipping first update")
    else:
        logging.warning("Shot ignored on BCT ring intenisty")


japc.subscribeParam(ring_bct, callback,
                    getHeader=True,
                    timingSelectorOverride=userName)

japc.startSubscriptions()

def plot_data(i):
    try:
        if len(bct_list) == len(timestamp_list):
            ax[0].cla()
            ax[1].cla()
            ax[2].cla()

            ax[0].plot(np.array(timestamp_list), np.array(bct_list), 'bo', label="BCTDC.31458")
            for i in bsi_dict.keys():
                ax[0].plot(np.array(timestamp_list), np.array(bsi_dict[i])/1e10, 'x', label=i)

            for i in blm_ba80_dict.keys():
                ax[1].plot(np.array(timestamp_list), np.array(blm_ba80_dict[i]), 'x', label=i.split("BLML.")[1].split("-UCAP")[0])

            ax[2].plot(np.array(timestamp_list), np.array(sum_blm_ba80), 'x')

            ax[0].set_xlabel(" Timestamp [UTC]")
            ax[1].set_xlabel(" Timestamp [UTC]")
            ax[2].set_xlabel(" Timestamp [UTC]")

            ax[0].set_ylabel("Intensity [1e10 p]")
            ax[1].set_ylabel("BLM / Gy")
            ax[2].set_ylabel("SUM BLM / Gy")

            ax[0].legend()
            ax[1].legend()

            plt.figure('ba80').suptitle("BA80 BLM monitor")
            plt.figure('ba80').tight_layout()

            plt.tight_layout()
    except:
        logging.warning("Length of arrays inconsistent")

def plot_data2(i):
    try:
        if len(bct_list) == len(timestamp_list):
            ax2[0].cla()
            ax2[1].cla()
            ax2[2].cla()

            ax2[0].plot(np.array(timestamp_list), np.array(bct_list), 'bo', label="BCTDC.31458")
            for i in bsi_dict.keys():
                ax2[0].plot(np.array(timestamp_list), np.array(bsi_dict[i])/1e10, 'x', label=i)

            for i in blm_tt20_dict.keys():
                ax2[1].plot(np.array(timestamp_list), np.array(blm_tt20_dict[i]), 'x', label=i.split("BLML.")[1].split("-UCAP")[0])

            ax2[2].plot(np.array(timestamp_list), np.array(sum_blm_tt20), 'x')

            ax2[0].set_xlabel("Timestamp [UTC]")
            ax2[1].set_xlabel("Timestamp [UTC]")
            ax2[2].set_xlabel("Timestamp [UTC]")

            ax2[0].set_ylabel("Intensity [1e10 p]")
            ax2[1].set_ylabel("BLM / Gy")
            ax2[2].set_ylabel("SUM BLM / Gy")

            ax2[0].legend()
            ax2[1].legend()

            plt.figure('tt20').suptitle("TT20 BLM monitor")
            plt.figure('tt20').tight_layout()

            plt.tight_layout()
    except:
        logging.warning("Length of arrays inconsistent")

fig, ax = plt.subplots(nrows=1, ncols=3, figsize=(14,6), num='ba80')
ani = FuncAnimation(fig, plot_data, interval=1000)

fig2, ax2 = plt.subplots(nrows=1, ncols=3, figsize=(14,6), num='tt20')
ani2 = FuncAnimation(fig2, plot_data2, interval=1000)

plt.show()